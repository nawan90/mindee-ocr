import React, { useState } from "react";
import {
  AnnotationLens,
  AnnotationShape,
  AnnotationViewer,
  PointerPosition
} from "react-mindee-js";
import dummyImage from "./surat1.jpg";

export const dummyShapes: AnnotationShape[] = [
  {
    id: "date",
    coordinates: [
      [0.289, 0.272],
      [0.793, 0.272],
      [0.793, 0.294],
      [0.289, 0.294]
    ]
  },
  {
    id: "supplier",
    coordinates: [
      [0.267, 0.212],
      [0.772, 0.212],
      [0.772, 0.252],
      [0.267, 0.252]
    ]
  },
  {
    id: "taxes",
    coordinates: [
      [0.198, 0.472],
      [0.500, 0.472],
      [0.500, 0.620],
      [0.198, 0.620]
    ]
  },
  {
    id: "time",
    coordinates: [
      [0.439, 0.677],
      [0.684, 0.677],
      [0.684, 0.780],
      [0.439, 0.780]
    ]
  },
  {
    id: "total_incl",
    coordinates: [
      [0.520, 0.366],
      [0.707, 0.366],
      [0.707, 0.380],
      [0.520, 0.380]
    ]
  }
];

export default function BasicExampleWithImage() {
  const [pointerPosition, setPointerPosition] = useState<PointerPosition>();
  const [annotationData] = useState({
    image: dummyImage,
    shapes: dummyShapes
  });
  const [eventData, setEventData] = useState({});
  const onShapeMouseEnter = (shape: AnnotationShape) => {
    setEventData({
      event: "mouseenter",
      shape
    });
  };
  const onShapeMouseLeave = (shape: AnnotationShape) => {
    setEventData({
      event: "mouseleave",
      shape
    });
  };
  const onShapeClick = (shape: AnnotationShape) => {
    setEventData({
      event: "click",
      shape
    });
  };
  const updatePointerPosition = (pointerPosition: PointerPosition) => {
    setPointerPosition(pointerPosition);
  };

  console.log;

  return (
    <div style={{ display: "flex", columnGap: 10 }}>
      <AnnotationViewer
        style={{ width: "70%", height: 600, background: "black" }}
        data={annotationData}
        getPointerPosition={updatePointerPosition}
        onShapeMouseLeave={onShapeMouseLeave}
        onShapeMouseEnter={onShapeMouseEnter}
        onShapeClick={onShapeClick}
      />
      <pre>{JSON.stringify(eventData, null, 2)}</pre>
 
      <AnnotationLens
        pointerPosition={pointerPosition}
        style={{
          width: "20vh",
          height: "20vh",
          margin: "auto",
          background: "black"
        }}
        data={annotationData}
      />
      
    </div>
  );
}
